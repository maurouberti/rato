unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, dxCntner, dxEditor, dxExEdtr,
  dxEdLib;

type
  TForm1 = class(TForm)
    LabelX: TLabel;
    LabelY: TLabel;
    Timer1: TTimer;
    bt_Gravar: TBitBtn;
    bt_Parar: TBitBtn;
    LabelTAM: TLabel;
    LabelTEMPO: TLabel;
    bt_Mover: TBitBtn;
    LabelTECLA: TLabel;
    Memo1: TMemo;
    dxSpinEdit_REPETIR: TdxSpinEdit;
    Label1: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure bt_GravarClick(Sender: TObject);
    procedure bt_PararClick(Sender: TObject);
    procedure bt_MoverClick(Sender: TObject);
  private
  protected
    procedure WndProc(var Message: TMessage); override;
  public
  end;


var
  Form1: TForm1;
  Gravar, Mover: Boolean;
  Pt: TPoint;
  Posicao: Integer;
  Vet_Posicoes: array of TPoint;
  Vet_Tempo: array of TTime;
  Vet_Teclas: Array of Integer;

implementation

{$R *.dfm}

procedure TForm1.WndProc(var Message: TMessage);
begin
   inherited WndProc(Message);
//   Windows.GetCursorPos(Pt);
end;


procedure TForm1.Timer1Timer(Sender: TObject);
var
   KeyLoop: Integer;
begin
   if Gravar then
   begin
      Windows.GetCursorPos(Pt);

      {Captura Posi��o do Mouse}
      Posicao := Length(Vet_Posicoes);
      SetLength(Vet_Posicoes, Posicao + 1);
      Vet_Posicoes[Posicao] := Pt;

      {Captura o Tempo}
      SetLength(Vet_Tempo, Posicao + 1);
      Vet_Tempo[Posicao] := Time;

      {Captura Teclas}
      SetLength(Vet_Teclas, Posicao + 1);
      Vet_Teclas[Posicao] := 0;
      KeyLoop := 0;
      repeat
         {Bot�o Esquerdo do Mouse / VK_LBUTTON = 1}
         if GetAsyncKeyState(KeyLoop) = -32767 then
         begin
            Vet_Teclas[Posicao] := KeyLoop;
//            Break;
         end;
         Inc(KeyLoop);
      until KeyLoop = 255;

   end
   else if Mover then
   begin
      Inc(Posicao);

      Windows.SetCursorPos(Vet_Posicoes[Posicao].X, Vet_Posicoes[Posicao].Y);

      if Vet_Teclas[Posicao] = 1 then
      begin
         mouse_event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
         mouse_event(MOUSEEVENTF_ABSOLUTE or MOUSEEVENTF_LEFTUP  , 0, 0, 0, 0);
      end
      else if Vet_Teclas[Posicao] <> 0 then
         keybd_event(Vet_Teclas[Posicao], 0, 0, 0);

      if Posicao = Length(Vet_Posicoes) then
      begin
         bt_Gravar.Enabled := True;
         bt_Parar.Enabled := False;
         bt_Mover.Enabled := True;
         Gravar := False;
         Mover := False;
         Timer1.Enabled := False;
         Posicao := 0;

         if dxSpinEdit_REPETIR.Value > 0 then
         begin
            dxSpinEdit_REPETIR.Value := dxSpinEdit_REPETIR.Value - 1;
            bt_MoverClick(nil);
         end;
      end;
   end;

   LabelX.Caption := 'x: '+IntToStr(Pt.X);
   LabelY.Caption := 'y: '+IntToStr(Pt.Y);
   LabelTAM.Caption := 'Tamanho: '+IntToStr(Posicao);
   LabelTEMPO.Caption := 'Tempo: '+FormatDateTime('NN:SS:ZZZ', Vet_Tempo[Posicao]-Vet_Tempo[0]);
   LabelTECLA.Caption := 'Tecla: ('+IntToStr(Vet_Teclas[Posicao])+') '+Chr(Vet_Teclas[Posicao]);

//   if Cancelar then bt_PararClick(nil);
end;

procedure TForm1.bt_GravarClick(Sender: TObject);
begin
   bt_Gravar.Enabled := False;
   bt_Parar.Enabled := True;
   bt_Mover.Enabled := False;
   Gravar := True;
   Mover := False;
   Timer1.Enabled := True;
   SetLength(Vet_Posicoes, 1);
   Vet_Posicoes[0] := Pt;
   SetLength(Vet_Tempo, 1);
   Vet_Tempo[0] := Time;
   SetLength(Vet_Teclas, 1);
   Vet_Teclas[0] := 0;
end;

procedure TForm1.bt_PararClick(Sender: TObject);
begin
   bt_Gravar.Enabled := True;
   bt_Parar.Enabled := False;
   bt_Mover.Enabled := True;
   Gravar := False;
   Mover := False;
   Timer1.Enabled := False;
end;

procedure TForm1.bt_MoverClick(Sender: TObject);
begin
   bt_Gravar.Enabled := False;
   bt_Parar.Enabled := True;
   bt_Mover.Enabled := False;
   Gravar := False;
   Mover := True;
   Timer1.Enabled := True;
   Posicao := 0;
end;

end.
